#include "matrices.c"

// Saves a double matrix in R
void conv_matrixd(double M[16][16]) {
  for (unsigned char i = 0; i < 13; i++)
    for (unsigned char j = 0; j < 13; j++)
      R[i][j] = (int) M[i][j];
}

// Saves a double matrix in R
void conv_matrixs(float M[16][16]) {
  for (unsigned char i = 0; i < 13; i++)
    for (unsigned char j = 0; j < 13; j++)
      R[i][j] = (int) M[i][j];
}

// Output Matlab-formatted result matrix
void print_matrix(int M[16][16]) {
  printf("[");
  for (unsigned char i = 0; i < 13; i++) {
    printf("[");
    for (unsigned char j = 0; j < 13; j++) {
      printf("%d",M[i][j]);
      if (j < 12 )
        printf(",");
    }
    printf("]");
    if (i < 12 )
      printf(";");
  }
  printf("]\n");
}

// Output Matlab-formatted result matrix
void print_matrixs(float M[16][16]) {
  printf("[");

  for (unsigned char i = 0; i < 13; i++) {
    printf("[");

    for (unsigned char j = 0; j < 13; j++) {
      printf("%g",M[i][j]);

      if (j < 12 )
        printf(",");
    }

    printf("]");

    if (i < 12 )
      printf(";");
  }

  printf("]\n");
}

// Check a matrix
void check_matrix(int M[16][16]) {
  for (unsigned char i = 0; i < 13; i++)
    for (unsigned char j = 0; j < 13; j++)
      if(M[i][j] != Correct[i][j]) {
        printf("=>Invalid result\n");
        return;
      }
  printf("=> Correct result\n");
}
