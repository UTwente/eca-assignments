// Clear result matrix and row/column 14 of X
void clear_matrix() {
  for (byte i = 0; i < 13; i++) {
    for (byte j = 0; j < 13; j++) {
      R[i][j] = 0;
    }
  }
}

// Output Matlab-formatted result matrix
void print_matrix(long M[13][13]) {
  Serial.print("[");

  for (byte i = 0; i < 13; i++) {
    Serial.print("[");

    for (byte j = 0; j < 13; j++) {
      Serial.print(M[i][j]);

      if (j < 12 )
        Serial.print(",");
    }

    Serial.print("]");

    if (i < 12 )
      Serial.print(";");
  }

  Serial.println("]");
}
