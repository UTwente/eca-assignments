#include <immintrin.h>
/*
void calc_mm_ssed() {
    unsigned char i,j,k;

    for (i = 0; i < 13; i++) {
        for (j = 0; j < 13; j++) {
            // This works with doubles
            __m128d rd = _mm_load_sd(&Yd[i][j]);
            for (k = 0; k < 13; k++) {
                __m128d x1d = _mm_load_sd(&Xd[i][k]);
                __m128d x2d = _mm_load_sd(&Xd[k][j]);
                rd = _mm_add_sd(_mm_mul_sd(x2d, x1d), rd);
            }
            _mm_store_sd(&Rd[i][j], rd);
        }
    }
}*/


void print_m256(__m256 p) {
  float t[8] __attribute__ ((aligned (32)));
  _mm256_stream_ps(&t[0], p);
  printf("[%g,%g,%g,%g,%g,%g,%g,%g]\n", t[0],t[1],t[2],t[3],t[4],t[5],t[6],t[7]);
}

void calc_avx() {
    unsigned char i,j,k;

    float Rt[16][16] __attribute__ ((aligned (16)));

    // Transpose matrix X (this can be optimised)
    for (i = 0; i < 16; i++)
        for (j = 0; j < 16; j++)
          Ts[i][j] = Xs[j][i];

    for (i = 0; i < 13; i++) {
        for (j = 0; j < 16; j+= 8) {
            __m256 z;
            float* zp = (float*) &z;
            for(k = 0; k < 8; k++) {
              // Load vector components of X & T
              __m256 x1 = _mm256_load_ps(&Xs[i][0]);
              __m256 x2 = _mm256_load_ps(&Xs[i][8]);
              __m256 t1 = _mm256_load_ps(&Ts[j+k][0]);
              __m256 t2 = _mm256_load_ps(&Ts[j+k][8]);

              // Create dot products
              __m256 r1 = _mm256_dp_ps(x1, t1, 0xf1);
              __m256 r2 = _mm256_dp_ps(x2, t2, 0xf1);

              // Add them together to get the result for R[i][j]
              __m256 rs  = _mm256_add_ps(r1, r2);
              __m128 rsl = _mm256_extractf128_ps(rs, 0);
              __m128 rsh = _mm256_extractf128_ps(rs, 1);
              __m128 rss = _mm_add_ps(rsl, rsh);

              _mm_store_ps(zp++, rss);
            }
            // Load 8 values from Y
            __m256 y = _mm256_load_ps(&Ys[i][j]);

            // Save the result
            _mm256_store_ps(&Rt[i][j], _mm256_add_ps(z,y));
        }
    }

    for (i = 0; i < 16; i++)
        for (j = 0; j < 16; j++)
          Rs[i][j] = Rt[i][j];

}


void print_m128i(__m128i p) {
  int t[4];
  for(int v=0;v<4;v++)
    t[v] = _mm_extract_epi32(p,v);

  printf("[%d,%d,%d,%d]\n", t[0],t[1],t[2],t[3]);
}

void calc_mm() {
    __m128i xr, xc, s1, s2, c, c1, c2, y;
    __m128i p[4], s[4];
    unsigned char i,j,k,l;
    char* rX;
    char* rT;
    char* rY;

    // Transpose matrix X and initialise R
    for (i = 0; i < 13; i++) {
      for (j = 0; j < 13; j++) {
        T[i][j] = X[j][i];
      }
    }

    for (i = 0; i < 13; i++) {
      for (j = 0; j < 13; j+=4) {
        rY = &Y[i][j];

        for (k = 0; k < 4; k++) {
          // Fill p with the products of all rows with columns,
          // every element of p contains four of the sixteen results.
          for(l = 0, rX = &X[i][0], rT = &T[j+k][0];
              l < 4;
              l++, rX += 4, rT += 4) {
            xr   = _mm_set_epi32(rX[3], rX[2], rX[1], rX[0]);
            xc   = _mm_set_epi32(rT[3], rT[2], rT[1], rT[0]);
            p[l] = _mm_mullo_epi32(xr, xc);
          }

          // Add adjacent values together
          // 16 => 8  => 4
          s1   = _mm_hadd_epi32(p[0], p[1]);
          s2   = _mm_hadd_epi32(p[2], p[3]);
          s[k] = _mm_hadd_epi32(s1, s2);
      }

      // Add more adjacent additions
      // 4 => 2 => 1
      c1 = _mm_hadd_epi32(s[0], s[1]);
      c2 = _mm_hadd_epi32(s[2], s[3]);
      c  = _mm_hadd_epi32(c1, c2);

      // Load elements from Y
      y = _mm_set_epi32(rY[3], rY[2], rY[1], rY[0]);

      // Add Y and store the result in R
      _mm_store_si128((__m128i *)&R[i][j], _mm_add_epi32(y, c));
    }
  }
}

void calc_simple() {
    unsigned char i,j,k;

    for (i = 0; i < 13; i++) {
        for (j = 0; j < 13; j++) {
            R[i][j] = Y[i][j];
            for (k = 0; k < 13; k++) {
                R[i][j] += X[i][k] * X[k][j];
            }
        }
    }
}

void calc_transpose() {
  unsigned char i,j,k;

  // Transpose matrix X and initialise R
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
      T[i][j] = X[j][i];
      R[i][j] = Y[i][j];
    }
  }

  // Multiply matrix X with T and add Y
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
        for (k = 0; k < 13; k++) {
          R[i][j] += X[i][k] * T[j][k];
      }
    }
  }
}

void calc_tile() {
  #define TS 4

  unsigned char i, i2, j, j2, k, k2;
  int* rR;
  char* rX1;
  char* rX2;

  // Calculate values not calculated in the tiling and add Y
  R[12][12] = X[12][12] * X[12][12] + Y[12][12];
  for (i = 0; i < 12; ++i) {
    R[i][12]   = X[i][12]  * X[12][12] + Y[i][12];
    R[12][i]   = X[12][12] * X[12][i]  + Y[12][i];
    R[12][12] += X[12][i]  * X[i][12];

    for (j = 0; j < 12; ++j) {
      R[i][j]   = X[i][12] * X[12][j] + Y[i][j];
      R[i][12] += X[i][j]  * X[j][12];
      R[12][i] += X[12][j] * X[j][i];
    }
  }

  // Multiply the inner 12x12 matrix in pieces of 4x4
  for (i = 0; i < 12; i += TS) {
    for (j = 0; j < 12; j += TS) {
      for (k = 0; k < 12; k += TS) {
        for (i2 = 0, rR = &R[i][j], rX1 = &X[i][k];
             i2 < TS; ++i2, rR += 13, rX1 += 13) {
          for (k2 = 0, rX2 = &X[k][j]; k2 < TS;
               ++k2, rX2 += 13) {
            for (j2 = 0; j2 < TS; ++j2) {
              //rR[j2] += rX1[k2] * rX2[j2];
              rR[j2] += X[i+i2][k+k2] * rX2[j2];
              //R[i+i2][j+j2] += X[i+i2][k+k2] * X[k+k2][j+j2];
            }
          }
        }
      }
    }
  }
}
