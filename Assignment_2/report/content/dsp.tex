\section{Digital signal processor}
\subsection{Introduction}
The goal in this assignment was to do matrix multiplications and additions with suitable processor architecture. It was observed that for implementing these kinds of applications, MAC (multiplier-accumulator) unit were best to use.  And so we chose TMS320C6678 device, which is a multi-core fixed and floating point DSP from Texas Instruments. It has 8 cores, each having 44.8 GMAC for fixed point and 22.4 GMAC for floating point operations\cite{dsp}. The maximum crystal frequency which can be supported with the device is 1.4GHz\cite{dsp}. We have used Code Composer Studio (CCS) v5 to simulate the device, which is an Integrated Development Environment (IDE) by Texas Instrument to support its various embedded processors. It supports both C/C++ and assembly level coding.  Even though assembly language provides a slightly higher optimization level than C, we chose to use C as the coding language because the efforts taken for optimization are very less\cite{dspopt}.
We are using the profile clock provided by CCS for accurate measurement of number of clocks used during execution of program.

To make such operations efficient with respect to program memory, data memory, usage of different cores and other resources we have tried different approaches to make the program work efficient. To make the compiler work smarter and more efficient, first we chose different algorithms like tiling the matrix by a factor of 4/8, transposing the matrix and reducing the number of multiplications. Then we tried to use some external DSP libraries which reduce the code size effectively and provide optimization. Lastly we worked with compiler optimization options which make the execution faster.


\subsection{Theory}

The hardware specifications of the device are summarized in table~\ref{tab:dsp:specs} in appendix~\ref{app:dsp:specs}. In the implementation, we are using all the cores to have maximum parallelism. As the maximum frequency is 1.4 GHz, it would be considered in all the calculations. The 8 functional units, .D, .S, .M, .L for each register A and B provide 3 stage instruction pipelining (fetch, decode and execute)\cite{dsp,dspopt}.

\subsection{Method}

\begin{enumerate}
    \item Reading matrix in blocks by tiling: In this method, we have implemented the loop-tiling concept. This means extra for loops are added in order to try and get the loops into the processor’s cache. As the processor has 8 functional units, we tried having two sizes of tiles 4x4 and 8x8 tiles. To implement this, for a 13x13 matrix there are some extra steps of calculations as it is not divisible by 4 or 8;
    \item Transposing the matrix: It was observed that the compiler pre-fetches data sequentially. In order to optimize the fetching of values from column vector, we transpose the matrix to make it sequential fetch.
    \item Reducing the number of multiplication: In the calculation of the diagonals, there are duplicate multiplication operations. In this method, the duplicate operations are done only once. Hence saving the number of multiplications. In the same method, we also reduce the number of loops by calculating the two triangles of matrix in one loop.
    \item Using DSP Library\cite{dsplib}: \\
        To optimize the code further, we used external libraries specific to DSP and matrix functionalities. We have pre-defined functions for matrix multiplications for different matrix size. These calculations are optimized using some pragma. Using such libraries reduces the code size to a great level. It also provides high level of optimization.
        \begin{itemize}
            \item Matrix as short: The elements of matrix are considered as short.
            \item Matrix as float: The element of matrix is considered as float.
            \item Matrix as double: For matrix elements as double, we have direct library function to calculate $Y = a \cdot X_1 \cdot X_2 + Y$ where $a$ is a scalar multiple.
        \end{itemize}
    \item Compiler options: In CCS, there are two ways of compiler optimizations \texttt{–opt\_level} and \texttt{–opt\_for\_space}. Both the methods have three levels of optimization. Using \texttt{opt\_level} makes sure that the compiler uses all the optimization techniques it can implement. And the \texttt{opt\_for\_space} is used in case of optimizing the code size for pointers and loops. In the results, we have used both the techniques simultaneously.
\end{enumerate}

\subsection{Execution}

\begin{table}[ht]
    \centering
    \begin{tabular}{l r r r r r r r r}
        \toprule
        & \multicolumn{4}{c}{Performance (cycles)}
        & \multicolumn{4}{c}{Performance (\si{\micro\second})} \\
        \cmidrule(r){2-5}
        \cmidrule(r){6-9}
        Method / Compiler setting & \texttt{3} & \texttt{2} & \texttt{1} & \texttt{0}
                                  & \texttt{3} & \texttt{2} & \texttt{1} & \texttt{0} \\
        \midrule
        Matrix as short & 1438 & 1421 & 3941 & 4436 & 1.02 & 1.01 & 2.81 & 3.16 \\
        Matrix as float & 292 & 308 & 1714 & 1727   & 0.20 & 0.22 & 1.22 & 1.23 \\
        Matrix as double & 188 & 172 & 188 & 182    & 0.13 & 0.12 & 0.13 & 0.13 \\
        \bottomrule
    \end{tabular}
    \caption{Performance of DSP library methods}
    \label{tab:dsp:perf:lib}

    \vspace{1ex}
    \begin{tabular}{l r r r r r r r r}
        \toprule
        & \multicolumn{4}{c}{Performance (cycles)}
        & \multicolumn{4}{c}{Performance (\si{\micro\second})} \\
        \cmidrule(r){2-5}
        \cmidrule(r){6-9}
        Method / Compiler setting & \texttt{3} & \texttt{2} & \texttt{1} & \texttt{0}
                                  & \texttt{3} & \texttt{2} & \texttt{1} & \texttt{0} \\
        \midrule
        tile\_by\_eight & 14243 & 14243 & 33267 & 76516           & 10.17 & 10.17 & 23.76 & 54.65 \\
        calc\_tile\_four & 10375 & 11659 & 53491 & 9719           & 7.41 & 8.32 & 38.2 & 6.94 \\
        calc\_triangle\_diagonal & 11742 & 11655 & 42869 & 65610  & 8.38 & 8.32 & 30.62 & 46.86 \\
        calc\_transpose & 7315 & 7315 & 20692 & 27632             & 5.22 & 5.22 & 14.78 & 19.73 \\
        simple\_calc (3 for loops) & 6089 & 6089 & 58941 & 83807  & 4.34 & 4.34 & 42.1 & 59.86 \\
        calc\_unrolling(2 for loops) & 2735 & 2735 & 9964 & 46290 & 1.95 & 1.95 & 7.11 & 33.06 \\
        \bottomrule
    \end{tabular}
    \caption{Performance of the other DSP calculation methods}
    \label{tab:dsp:perf:othert}
\end{table}


All methods are simulated in Code Composer Studio v5. Results  Several things can be noted:

\begin{enumerate}
    \item The compiler optimization technique does not give a significant change when we use DSP library for the matrix as double. This might be because the functional units are 64 bit. And the doubles are already operating at an optimized level for 64 bit operations.
    \item Using compiler optimization shows a significant performance achievement for simple matrix multiplication in 3 loops and the \texttt{loop\_unrolling} method. This is because these methods are very simple to implement and compiler has some space to implement software optimization methods intrinsically.
    \item In the tiling methods, we do not see much improvement because these methods are already code optimized.
    \item DSP libraries are the best optimized coding method as it reduces the code in five lines and gives the fastest performance.
\end{enumerate}





