#!/bin/sh
program="$1"
./make.sh

for i in $@; do
  LC_NUMERIC=en_US.UTF8 perf stat -B $i
done
