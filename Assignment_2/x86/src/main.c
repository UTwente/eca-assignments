//
//  main.c
//  test
//
//  Created by Silke Hofstra on 2016-10-24.
//  Copyright © 2016 Silke Hofstra. All rights reserved.
//
#define L 1000000
#define C 32
#define N 13
#define CALC calc_avx
#define TYPE 0// 0: int, 1: float, 2:double


#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <pthread.h>

#include "matrix.c"
#include "debug.c"
#include "math.c"

// Thread array
pthread_t tid[C];

#define SM (CLS / sizeof (int))

void* thread(void *loops) {
    //float MR[16][16] __attribute__ ((aligned (32)));
    int l = *((int *) loops);
    for (int f = 0; f < l; f++) {
        CALC();
    }

    return NULL;
}

void bench(int threads) {
    int i, loops;
    struct timeval start, end;
    double taken;

    // Save start time
    printf("Start. Threads: %d\n", threads);
    gettimeofday(&start, NULL);

    // Create threads
    for(i = 0; i < threads; i++) {
        loops = L / threads;
        pthread_create(&(tid[i]), NULL, &thread, (void *) &loops);
    }

    // Wait for threads to finish
    for(i = 0; i < threads; i++) {
        pthread_join(tid[i], NULL);
    }

    // Calculate time taken
    gettimeofday(&end, NULL);
    taken = ((end.tv_sec  - start.tv_sec) * 1000000u +
              end.tv_usec - start.tv_usec) / 1.e6;

    // Show statistics
    printf("Time taken (s): %2.4f\n", taken);
    printf("Time/calc (ns): %2.4f\n", taken/L*1.e9);

    // Convert matrix to integer
    #if TYPE == 1
        conv_matrixs(Rs);
    #elif TYPE == 2
        conv_matrixd(Rd);
    #endif

    // Check result
    check_matrix(R);
}

int main (void) {
    printf("Calculations: %d\n", L);

    bench(1);
    bench(4);
    bench(32);
    return 0;
}
