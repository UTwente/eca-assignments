#!/bin/sh

md="presentation.md"
odp="presentation.odp"

odpdown -s emacs -p 4 \
  --break-master=template1 \
  --content-master=template \
  "${md}" "template.odp" "${odp}"

soffice --headless --convert-to pdf:impress_pdf_Export "${odp}"
