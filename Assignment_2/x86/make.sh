#!/bin/bash
compile() {
  echo Compiling $1
  gcc "src/$1.c" -o "bin/$1" -Wall -lpthread -march=native -O3
}

compile main
code=$?
[ "$1" = "main" ] && exit $code
[ $code -ne 0 ] && exit $code

for i in simple avx sse; do
  for n in 1 4 32; do
    compile "${i}_${n}"
    code=$?
    [ $code -ne 0 ] && exit $code
  done
done
