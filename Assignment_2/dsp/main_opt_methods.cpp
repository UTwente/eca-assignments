/*
 * main.c
 */
#include <stdio.h>

long R1[13][13];
long R2[13][13];
long R3[13][13];
long R4[13][13];
long R5[13][13];
long R6[13][13];

void simple_calc(); // calculations done using 3 for loops
void calc_triangle_diagonal(); // calculate diagonals(reduce mul) and the triangles(reduce loops) separately
void calc_transpose(); // perform X*X by taking transpose of X and then row-wise dot operation.
void calc_tile_four(); // tile size 4
void calc_unrolling(); // use 2 for loops
void tile_by_eight(); // tile size 8

char X[13][13] = { { 4, 78, 42, 112, 77, 4, 92, 126, 29, 95, 34, 116, 109 }, {
		65, 108, 113, 126, 50, 12, 119, 60, 45, 32, 107, 79, 102 }, { 15, 46,
		102, 41, 125, 121, 120, 62, 40, 22, 112, 94, 115 }, { 13, 22, 66, 21, 7,
		25, 4, 31, 93, 104, 110, 61, 55 }, { 108, 123, 115, 20, 72, 88, 115, 0,
		86, 0, 92, 70, 16 }, { 12, 35, 6, 111, 70, 76, 52, 58, 62, 52, 54, 90,
		71 }, { 8, 74, 35, 1, 47, 25, 40, 99, 2, 55, 93, 3, 12 }, { 38, 31, 52,
		43, 127, 97, 4, 127, 23, 54, 67, 4, 13 }, { 79, 67, 32, 20, 59, 25, 40,
		119, 62, 77, 124, 114, 71 }, { 75, 24, 90, 11, 94, 105, 56, 51, 55, 64,
		55, 11, 31 }, { 14, 1, 45, 110, 52, 102, 36, 89, 113, 106, 89, 34, 64 },
		{ 2, 94, 58, 95, 34, 126, 11, 88, 24, 119, 54, 108, 17 }, { 127, 26, 39,
				85, 81, 126, 88, 66, 63, 41, 110, 95, 102 } };
char Y[13][13] = { { 80, 72, 58, 7, 122, 78, 87, 116, 20, 127, 21, 24, 44 }, {
		109, 49, 102, 110, 42, 0, 113, 86, 12, 26, 110, 34, 53 }, { 82, 21, 97,
		30, 10, 36, 5, 19, 113, 74, 123, 36, 1 }, { 46, 120, 84, 97, 86, 8, 22,
		98, 126, 120, 44, 2, 58 }, { 73, 36, 106, 87, 32, 118, 38, 26, 115, 31,
		65, 111, 2 }, { 0, 66, 89, 65, 4, 88, 11, 47, 25, 74, 87, 19, 119 }, {
		43, 46, 50, 42, 1, 24, 127, 6, 123, 4, 2, 83, 81 }, { 114, 79, 26, 3,
		13, 59, 1, 103, 64, 116, 124, 21, 70 }, { 15, 97, 69, 121, 66, 90, 71,
		59, 107, 49, 0, 5, 15 }, { 115, 115, 40, 5, 57, 44, 2, 14, 27, 58, 74,
		111, 108 }, { 14, 65, 21, 70, 11, 49, 83, 51, 94, 16, 23, 61, 49 }, {
		118, 43, 63, 91, 115, 26, 127, 102, 126, 119, 105, 51, 25 }, { 35, 84,
		71, 48, 35, 21, 124, 114, 44, 97, 83, 14, 105 } };

// considering 16x16 matrix for tile size 8
char X_sq[16][16] = { { 4, 78, 42, 112, 77, 4, 92, 126, 29, 95, 34, 116, 109, 0,
		0, 0 }, { 65, 108, 113, 126, 50, 12, 119, 60, 45, 32, 107, 79, 102, 0,
		0, 0 }, { 15, 46, 102, 41, 125, 121, 120, 62, 40, 22, 112, 94, 115, 0,
		0, 0 }, { 13, 22, 66, 21, 7, 25, 4, 31, 93, 104, 110, 61, 55, 0, 0, 0 },
		{ 108, 123, 115, 20, 72, 88, 115, 0, 86, 0, 92, 70, 16, 0, 0, 0 }, { 12,
				35, 6, 111, 70, 76, 52, 58, 62, 52, 54, 90, 71, 0, 0, 0 }, { 8,
				74, 35, 1, 47, 25, 40, 99, 2, 55, 93, 3, 12, 0, 0, 0 }, { 38,
				31, 52, 43, 127, 97, 4, 127, 23, 54, 67, 4, 13, 0, 0, 0 }, { 79,
				67, 32, 20, 59, 25, 40, 119, 62, 77, 124, 114, 71, 0, 0, 0 }, {
				75, 24, 90, 11, 94, 105, 56, 51, 55, 64, 55, 11, 31, 0, 0, 0 },
		{ 14, 1, 45, 110, 52, 102, 36, 89, 113, 106, 89, 34, 64, 0, 0, 0 }, { 2,
				94, 58, 95, 34, 126, 11, 88, 24, 119, 54, 108, 17, 0, 0, 0 },
		{ 127, 26, 39, 85, 81, 126, 88, 66, 63, 41, 110, 95, 102, 0, 0, 0 }, {
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

int main(void) {

	simple_calc(); //Clock_cycle--opt_level: 6089--3	6089--2	  58941--1	83807--0

	calc_triangle_diagonal(); //Clock_cycle--opt_level: 11742--3	11655--2	42869--1	65610--0

	calc_transpose(); //Clock_cycle--opt_level: 7315--3 	7315--2 	20692--1 	27632--0

	calc_tile_four(); //Clock_cycle--opt_level: 10375--3 	11659--2 	53491--1 	9719--0

	calc_unrolling(); //Clock_cycle--opt_level: 2735--3 	2735--2 	9964--1 	46290--0

	tile_by_eight(); //Clock_cycle--opt_level: 14243--3 	14243--2 	33267--1 	76516--0

}
/**
 * simple calculation using 3 for loops
 */
void simple_calc() {
	int i, j, k;
	for (i = 0; i < 13; i++) {
		for (j = 0; j < 13; j++) {
			R1[i][j] = Y[i][j];
			for (k = 0; k < 13; k++) {
				R1[i][j] += X[i][k] * X[k][j];
			}
		}
	}

}
/**
 * calculate transpose T of X. Then perform X*X by serial dot product of X and T.
 */
void calc_transpose() {
	int i, j, k;
	long T[13][13];
	// Transpose matrix X and initialize R
	for (i = 0; i < 13; i++) {
		for (j = 0; j < 13; j++) {
			T[i][j] = X[j][i];
			R2[i][j] = Y[i][j];
		}
	}
	// Multiply matrix X with T and add Y
	for (i = 0; i < 13; i++) {
		for (j = 0; j < 13; j++) {
			for (k = 0; k < 13; k++) {
				R2[i][j] += X[i][k] * T[j][k];
			}
		}
	}
}

/**
 * tile size 4
 * using the 12x12 matrix to tile.
 * Other part of the calculations done separately
 */
void calc_tile_four() {
#define TS 4
	char i, i2, j, j2, k, k2;
	long* rR;
	char* rX1;
	char* rX2;
	// Calculate values not calculated in the tiling and add Y
	R3[12][12] = X[12][12] * X[12][12] + Y[12][12];
	for (i = 0; i < 12; ++i) {
		R3[i][12] = X[i][12] * X[12][12] + Y[i][12];
		R3[12][i] = X[12][12] * X[12][i] + Y[12][i];
		R3[12][12] += X[12][i] * X[i][12];
		for (j = 0; j < 12; ++j) {
			R3[i][j] = X[i][12] * X[12][j] + Y[i][j];
			R3[i][12] += X[i][j] * X[j][12];
			R3[12][i] += X[12][j] * X[j][i];
		}
	}
	// Multiply the inner 12x12 matrix in pieces of 4x4
	for (i = 0; i < 12; i += TS) {
		for (j = 0; j < 12; j += TS) {
			for (k = 0; k < 12; k += TS) {
				for (i2 = 0, rR = &R3[i][j], rX1 = &X[i][k]; i2 < TS;
						++i2, rR += 13, rX1 += 13) {
					for (k2 = 0, rX2 = &X[k][j]; k2 < TS; ++k2, rX2 += 13) {
						for (j2 = 0; j2 < TS; ++j2) {
							//rR[j2] += rX1[k2] * rX2[j2];
							rR[j2] += X[i + i2][k + k2] * rX2[j2];
							//R[i+i2][j+j2] += X[i+i2][k+k2] * X[k+k2][j+j2];
						}
					}
				}
			}
		}
	}
}

/**
 * diagonals and triangles calculated separately
 * calculation of diagonals is done to reduce multiplications
 * calculations of two triangles performed in a single loop.
 */
void calc_triangle_diagonal() {
#define N 12
	int a, b, i, j;
	long temp;
	// Initialise diagonals
	for (a = 0; a < N; ++a) {
		R4[a][a] = Y[a][a];
	}
	// Calculate diagonals
	for (a = 0; a < N; ++a) {
		R4[a][a] += X[a][a] * X[a][a];
		for (b = a + 1; b <= N; ++b) {
			temp = X[a][b] * X[b][a];
			R4[a][a] += temp;
			R4[b][b] += temp;
		}
	}
	// Initialise corners
	R4[N][N] = Y[N][N] + X[N][N] * X[N][N];
	// Calculate the remaining half triangles
	for (i = 1; i <= N; ++i) {
		for (j = 0; j < i; ++j) {
			R4[i][j] = Y[i][j];
			R4[j][i] = Y[j][i];
			for (a = 0; a <= N; ++a) {
				R4[i][j] += X[i][a] * X[a][j];
				R4[j][i] += X[j][a] * X[a][i];
			}
		}
	}
}
/**
 * unrolling one for loop.
 * calculations done with 2 for loops
 */
void calc_unrolling() {
	int i, j;
	for (i = 0; i < 13; ++i) {
		for (j = 0; j < 13; ++j) {
			R5[i][j] = Y[i][j];
			R5[i][j] += X[i][1] * X[1][j];
			R5[i][j] += X[i][2] * X[2][j];
			R5[i][j] += X[i][3] * X[3][j];
			R5[i][j] += X[i][4] * X[4][j];
			R5[i][j] += X[i][5] * X[5][j];
			R5[i][j] += X[i][6] * X[6][j];
			R5[i][j] += X[i][7] * X[7][j];
			R5[i][j] += X[i][8] * X[8][j];
			R5[i][j] += X[i][9] * X[9][j];
			R5[i][j] += X[i][10] * X[10][j];
			R5[i][j] += X[i][11] * X[11][j];
			R5[i][j] += X[i][12] * X[12][j];

		}

	}
}
/**
 * tile size 8.
 * Using the X matrix as 16x16 matrix for calculations.
 *
 */
void tile_by_eight() {
#define TSize 8;
	int i, a, b, i2, j, j2, k, k2;
	long* rR;
	char* rX1;
	char* rX2;
	for(a=0;a<13;a++){
		for(b=0;b<13;b++){
			R6[j][k]=Y[j][k];
		}
	}

	for (i = 0; i < 16; i += 8) {
		for (j = 0; j < 16; j += 8) {
			for (k = 0; k < 16; k += 8) {
				for (i2 = 0, rR = &R6[i][j], rX1 = &X_sq[i][k]; i2 < 8;
						++i2, rR += 16, rX1 += 16) {
					for (k2 = 0, rX2 = &X_sq[k][j]; k2 < 8; ++k2, rX2 += 16) {
						for (j2 = 0; j2 < 8; ++j2) {
							rR[j2] += X_sq[i + i2][k + k2] * rX2[j2];
						}
					}
				}
			}
		}
	}
}

