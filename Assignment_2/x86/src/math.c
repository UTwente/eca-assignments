#include <immintrin.h>
void transpose_s(float Ts[16][16]) {
    for (int i = 0; i < 13; i++)
        for (int j = 0; j < 13; j++)
            Ts[i][j] = Xs[j][i];
}

void transpose_avx(float Ts[16][16] ) {
  for (int i = 0; i < 16; i+=4) {
    for (int j = 0; j < 16; j+=4) {
      // Load four rows
      __m128 t[4];
      for (int k = 0; k < 4; k++)
        t[k] = _mm_load_ps(&Xs[i+k][j]);

      // Transpose them
      _MM_TRANSPOSE4_PS(t[0], t[1], t[2], t[3]);

      // Save them
      for (int k = 0; k < 4; k++)
        _mm_store_ps(&Ts[j+k][i], t[k]);
    }
  }
}

void calc_avx() {
    // Transpose matrix X
    float Ts[16][16] __attribute__ ((aligned (32)));
    transpose_avx(Ts);

    // Pointer to X
    float* rX = &Xs[0][0];
    for (int i = 0; i < 13; i++, rX+=16) {
        // Load vector components of X
        __m256 x1 = _mm256_load_ps(&rX[0]);
        __m256 x2 = _mm256_load_ps(&rX[8]);

        for (int j = 0; j < 16; j+= 8) {
            // Z vector for components of X*X
            __m256 z;

            // Pointer to the z vector
            float* zp = (float*) &z;

            // Pointer to T (X')
            float* rT = &Ts[j][0];
            for(int k = 0; k < 8; k++, rT += 16, zp++) {
              // Load vector components of T
              __m256 t1 = _mm256_load_ps(&rT[0]);
              __m256 t2 = _mm256_load_ps(&rT[8]);

              // Create dot products
              __m256 r1 = _mm256_dp_ps(x1, t1, 0xf1);
              __m256 r2 = _mm256_dp_ps(x2, t2, 0xf1);

              // Add them together to get the result for R[i][j]
              __m256 rs  = _mm256_add_ps(r1, r2);
              __m128 rsl = _mm256_extractf128_ps(rs, 0);
              __m128 rsh = _mm256_extractf128_ps(rs, 1);
              __m128 rss = _mm_add_ps(rsl, rsh);

              // Store the result in z
              _mm_store_ps(zp, rss);
            }
            // Load 8 values from Y
            __m256 y = _mm256_load_ps(&Ys[i][j]);

            // Save the result
            _mm256_store_ps(&Rs[i][j], _mm256_add_ps(z,y));
        }
    }
}

void transpose(char T[16][16]) {
  for (int i = 0; i < 13; i++)
    for (int j = 0; j < 13; j++)
      T[i][j] = X[j][i];
}

void calc_sse() {
  // Transpose matrix X and initialise R
  char T[16][16];
  transpose(T);

  char* rX = &X[0][0];
  for (int i = 0; i < 16; i++, rX += 16) {
    // Load 16 rows from memory
    __m128i xr1 = _mm_set_epi16(rX[7], rX[6], rX[5], rX[4],
                                rX[3], rX[2], rX[1], rX[0]);
    __m128i xr2 = _mm_set_epi16(rX[15], rX[14], rX[13], rX[12],
                                rX[11], rX[10], rX[9],  rX[8]);
    // Loop through columns
    for (int j = 0; j < 16; j+=4) {
      __m128i s[4];
      char* rY = &Y[i][j];
      char* rT = &T[j][0];
      for(int k = 0; k < 4; k++, rT += 16) {
        // Load 16 column values from memory
        __m128i xc1 = _mm_set_epi16(rT[7], rT[6], rT[5], rT[4],
                                    rT[3], rT[2], rT[1], rT[0]);
        __m128i xc2 = _mm_set_epi16(rT[15], rT[14], rT[13], rT[12],
                                    rT[11], rT[10], rT[9],  rT[8]);

        // Multiply accumulate to two times four 32 bit values
        __m128i p1 = _mm_madd_epi16(xr1, xc1);
        __m128i p2 = _mm_madd_epi16(xr2, xc2);

        // Add adjacent together to four values
        s[k] = _mm_hadd_epi32(p1, p2);
      }

      // Add more adjacent values
      // 4 => 2 => 1, four values of R
      __m128i c = _mm_hadd_epi32(
                    _mm_hadd_epi32(s[0], s[1]),
                    _mm_hadd_epi32(s[2], s[3])
                  );

      // Load elements from Y
      __m128i y = _mm_set_epi32(rY[3], rY[2], rY[1], rY[0]);

      // Add Y and store the result in R
      _mm_store_si128((__m128i *)&R[i][j], _mm_add_epi32(y, c));
    }
  }
}

void calc_simple() {
    unsigned char i,j,k;

    for (i = 0; i < 13; i++) {
        for (j = 0; j < 13; j++) {
            R[i][j] = Y[i][j];
            for (k = 0; k < 13; k++) {
                R[i][j] += X[i][k] * X[k][j];
            }
        }
    }
}

void calc_transpose() {
  unsigned char i,j,k;

  // Transpose matrix X and initialise R
  char T[13][13];
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
      T[i][j] = X[j][i];
      R[i][j] = Y[i][j];
    }
  }

  // Multiply matrix X with T and add Y
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
        for (k = 0; k < 13; k++) {
          R[i][j] += X[i][k] * T[j][k];
      }
    }
  }
}

void calc_tile() {
  #define TS 4

  unsigned char i, i2, j, j2, k, k2;
  int* rR;
  char* rX1;
  char* rX2;

  // Calculate values not calculated in the tiling and add Y
  R[12][12] = X[12][12] * X[12][12] + Y[12][12];
  for (i = 0; i < 12; ++i) {
    R[i][12]   = X[i][12]  * X[12][12] + Y[i][12];
    R[12][i]   = X[12][12] * X[12][i]  + Y[12][i];
    R[12][12] += X[12][i]  * X[i][12];

    for (j = 0; j < 12; ++j) {
      R[i][j]   = X[i][12] * X[12][j] + Y[i][j];
      R[i][12] += X[i][j]  * X[j][12];
      R[12][i] += X[12][j] * X[j][i];
    }
  }

  // Multiply the inner 12x12 matrix in pieces of 4x4
  for (i = 0; i < 12; i += TS) {
    for (j = 0; j < 12; j += TS) {
      for (k = 0; k < 12; k += TS) {
        for (i2 = 0, rR = &R[i][j], rX1 = &X[i][k];
             i2 < TS; ++i2, rR += 13, rX1 += 13) {
          for (k2 = 0, rX2 = &X[k][j]; k2 < TS;
               ++k2, rX2 += 13) {
            for (j2 = 0; j2 < TS; ++j2) {
              //rR[j2] += rX1[k2] * rX2[j2];
              rR[j2] += X[i+i2][k+k2] * rX2[j2];
              //R[i+i2][j+j2] += X[i+i2][k+k2] * X[k+k2][j+j2];
            }
          }
        }
      }
    }
  }
}
