#!/bin/sh
program="$1"
./make.sh

for i in $@; do
  valgrind --tool=cachegrind --cachegrind-out-file=cachegrind/$i.out ./bin/$i
done
