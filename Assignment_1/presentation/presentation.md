## Transposition

``` C
for (i = 0; i < 13; i++)
  for (j = 0; j < 13; j++) {
    T[i][j] = X[j][i];
    R[i][j] = Y[i][j];
  }
for (i = 0; i < 13; i++)
  for (j = 0; j < 13; j++)
      for (k = 0; k < 13; k++)
        R[i][j] += X[i][k] * T[j][k];
```

*Marginal improvement*

## Tiling (2/2)

``` C
for (i = 0; i < 12; i += TS)
  for (j = 0; j < 12; j += TS)
    for (k = 0; k < 12; k += TS)
      for (i2 = 0, rR = &R[i][j],
           rX1 = &X[i][k]; i2 < TS; ++i2,
           rR += 13, rX1 += 13)
        for (k2 = 0, rX2 = &X[k][j];
             k2 < TS; ++k2, rX2 += 13)
          for (j2 = 0; j2 < TS; ++j2)
            rR[j2] += rX1[k2] * rX2[j2];
```

*No improvement*

## Minimizing multiplications

``` C
R1,1 = (X1,1 * X1,1) + (X1,2 * X2,1) + (X1,3 * X3,1) … + (X1,13 * X13,1)
R2,2 = (X1,2 * X2,1) + (X2,2 * X2,2) + (X2,3 * X3,2) … + (X2,13 * X13,2)
R2,2 = (X1,3 * X3,1) + (X2,3 * X3,2) + (X3,3 * X3,3) … + (X3,13 * X13,3)
…
R13,13 = …
```

**Reduction in calculation:**
For 13 x 13 matrix, number of repetitive multiplications = 13 C_2 = 78

``` C
for (a = 0; a < N; ++a) {
    R[a][a] += X[a][a] * X[a][a];
    for (b = a + 1; b <= N; ++b) {
      temp = X[a][b] * X[b][a];
      R[a][a] += temp;
      R[b][b] += temp;
    }
  }
```

## Minimizing loops
``` C
for (i = 2; i <= N; ++i)
    for (j = 1; j < i; ++j)
     // Calculate R[i][j] and R[j][i];
```
