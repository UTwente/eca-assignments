#include <immintrin.h>

void print_m256(__m256 p) {
  float t[8] __attribute__ ((aligned (32)));
  _mm256_stream_ps(&t[0], p);
  printf("[%g,%g,%g,%g,%g,%g,%g,%g]\n", t[0],t[1],t[2],t[3],t[4],t[5],t[6],t[7]);
}

void print_m128(__m128 p) {
  float t[8] __attribute__ ((aligned (32)));
  _mm_stream_ps(&t[0], p);
  printf("[%g,%g,%g,%g]\n", t[0],t[1],t[2],t[3]);
}

void print_m128i(__m128i p) {
  int t[4];
  for(int v=0;v<4;v++)
    t[v] = _mm_extract_epi32(p,v);

  printf("[%d,%d,%d,%d]\n", t[0],t[1],t[2],t[3]);
}
