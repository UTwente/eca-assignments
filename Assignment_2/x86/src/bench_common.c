#define LOOPS 1000000

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "matrix.c"
#include "math.c"

// Thread array
pthread_t tid[THREADS];

void* thread(void *args) {
    for (int f = 0; f < (LOOPS/THREADS); f++) {
      CALC();
    }

    return NULL;
}

int main (void) {
    int i;

    // Create threads
    for(i = 0; i < THREADS; i++) {
        pthread_create(&(tid[i]), NULL, &thread, NULL);
    }

    // Wait for threads to finish
    for(i = 0; i < THREADS; i++) {
        pthread_join(tid[i], NULL);
    }

    return 0;
}
