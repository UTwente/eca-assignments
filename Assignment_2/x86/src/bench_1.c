#define LOOPS 1000000

#include <stdlib.h>
#include <stdio.h>

#include "matrix.c"
#include "math.c"

int main (void) {
    for (int f = 0; f < LOOPS; f++) {
        CALC();
    }

    return 0;
}
