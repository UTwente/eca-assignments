\section{General purpose processor}
\subsection{Introduction}
% \begin{wraptable}{r}{40mm}
%   \centering
%   \begin{tabular}{r r}
%     \toprule
%     Level & Size (\si{kB}) \\
%     \midrule
%     1 & 256 \\
%     2 & 1024 \\
%     3 & 6144 \\
%     \bottomrule
%   \end{tabular}
%   \caption{Cache levels of the used processor}
%   \label{tab:cache}
% \end{wraptable}

The processor used for this part is an Intel Core i5-3570K\cite{i5}.
This is an x86 (CISC) architecture.
The most important features are:

\begin{itemize}
  \item Clock frequency of \SI{3.40}{GHz} to \SI{3.80}{GHz}.
  \item Three cache layers.
  \item Extensions: SSE 4.1, 4.2 and AVX.
\end{itemize}

Because of SSE (Streaming SIMD Extensions) and AVX (Advances Vector Extensions),
the processor has build-in instructions to create vectors.

\subsection{Theory}
A matrix calculation $X\cdot Y$ results in fields which are the result of the dot product of the row- and column-vector of the input matrices.
This means that in the resulting matrix $R$, $R_{5,6}$ is the result of the dot product of the fifth row and sixth column.

Because a matrix multiplication is effectively a number of vector operations,
a GPP with support for vector operations should be able to gain significant performance gains, even on a 13 by 13 matrix.

Additional improvement can be gained by exploiting multi-threading.

\subsection{Method}
There are two main ways of implementing the matrix multiplication with the given GPP:
using a 128 bit vector with 16 or 32 bit components or using a 256 bit vector with 32 bit floating point components. The performance is compared to a simple matrix multiplication algorithm.

The implementation is written in \emph{C}, using the intrinsics\cite{intrinsics} available for SSE/AVX.

For multithreading the POSIX thread library \texttt{pthread} is used.
Threads are created and joined in simple for loops.
No measures are taken against hazards when writing to the result matrix $R$.
Cases are made with 1, 4 and 32 threads. Performance is measured in several ways:

\begin{itemize}
  \item \texttt{main.c}, which tries a varying number of threads and measures the time the calculations take. This is measured for \num{1000000} calculations.
  \item Valgrind's \emph{cachegrind}, which shows information about instructions and cache access.
  \item Linux's \emph{perf stat}, which shows runtime information about a process.
\end{itemize}

For each way of calculating the code is compiled using GCC with options \texttt{-Wall -march=native -O3}. This shows warnings, compiles for the entire architecture of the GPP compiling the code and optimizes for fast execution.

\subsubsection{Integer: SSE}
The integer calculation is the hardest, because the intrinsics used for manipulating vectors do not easily support multiplications or memory access.
The calculation, implemented with \texttt{calc\_sse} and \texttt{transpose} (see appendix~\ref{app:x86:math.c}), is done in several steps:

\begin{itemize}
  \item Transpose X so vectors can be sequentially read from memory.
  \item Load one row from X as two 128 bits vectors with 16 bit elements.
  \item Loop trough the columns:
  \begin{itemize}
    \item Load a column as two 128 bits vectors with 16 bit elements.
    \item Multiply the row and column vectors and add adjacent pairs resulting in two 128 bits vectors with 32 bit elements.
    \item Keep adding adjacent pairs a couple of more times to produce the result.
    \item Load one row from Y as one 128 bit vector with 32 bit elements.
    \item Add the result and Y and store them in one row of result matrix R.
  \end{itemize}
\end{itemize}

The main problem with this approach is that loading the vectors from memory is done by loading separate elements, not reading a block of memory.

\subsubsection{Floating point: AVX}
For this approach the input matrix is stored as a floating point matrix instead of signed integers.
The calculation is implemented with \texttt{calc\_avx} and \texttt{transpose\_avx} (see appendix~\ref{app:x86:math.c}).

Because single precision floating point numbers use 23 bits to store the value of the number (and 1 for the sign) and the maximum value of the result is only 17 bits no precision will be lost.
The approach with floating point numbers is a lot more straight forward than with integers:

\begin{itemize}
  \item Transpose $X$ so vectors can be sequentially read from memory in blocks of 4.
  \item Load one row from $X$ as two 256 bits vectors with 32 bit floating point elements.
  \item Loop trough the columns:
  \begin{itemize}
    \item Load a column as two 256 bits vectors with 32 bit floating point elements.
    \item Calculate the dot product of the 128 bit components of the vectors.
    \item Add the results from the dot products together to get a 128 bit vector containing only one value.
    \item Store this value in a temporary 256 bit vector $Z$.
    \item Load half a row from $Y$ as one 256 bits vector with 32 bit floating point elements.
    \item Add $Z$ and $Y$ and store them in half a row of result matrix $R$.
  \end{itemize}
\end{itemize}

This approach has some inefficiencies (vectors are being added while they contain only one value),
but the code is relatively easy to understand.

\subsection{Execution}

\begin{table}[ht]
  \centering
  \sisetup{round-mode=places,round-precision=2}
  \begin{tabular}{l r r r r r}
    \toprule
    Algoritm & Threads & Time (\si{ns}) & Instructions & Cycles & Stalled Cycles \\
    \midrule
    Simple &  1 &  706 &  \num{8328.100256} & \num{2671.090095} &  \num{607.246703} \\
           &  4 &  199 &  \num{8328.215049} & \num{2699.342914} &  \num{630.821752} \\
           & 32 &  192 &  \num{8328.268868} & \num{2698.323350} &  \num{632.579353} \\
    SSE    &  1 & 1614 & \num{12904.100247} & \num{6015.991848} & \num{1991.603603} \\
           &  4 &  468 & \num{12904.215013} & \num{6012.000868} & \num{1986.664366} \\
           & 32 &  446 & \num{12904.268602} & \num{6016.496474} & \num{1989.450645} \\
    AVX    &  1 &  536 &  \num{2683.100247} & \num{2117.374999} &  \num{794.690966} \\
           &  4 &  230 &  \num{2683.215013} & \num{2970.627544} & \num{1557.125356} \\
           & 32 &  225 &  \num{2683.268580} & \num{3146.242640} & \num{1756.740411} \\
    \bottomrule
  \end{tabular}
  \caption{Results of 1 million calculations normalised to 1 calculation on the GPP.}
  \label{tab:gpp:res}
\end{table}

The results of the implementations, normalised to a single calculation, can be seen in table~\ref{tab:gpp:res}.
Statistics for the execution are shown in table~\ref{tab:gpp:stats}.

The AVX implementation is the fastest for a single thread, offering 24\% improvement.
The SSE implementation is noticeably slower than the simple and AVX implementation,
probably because of lack of optimisation of the used intrinsics when compiling to instructions.
Because of this the instruction count is higher and because the executed instructions/cycle are lower this causes degraded performance.

\begin{wraptable}{r}{65mm}
  \centering
  \begin{tabular}{l r r r}
    \toprule
    Algoritm & Th. & Inst./C. & Stall. (\%) \\
    \midrule
    Simple &  1 & 3.12 & 22.73 \\
           &  4 & 3.09 & 23.37 \\
           & 32 & 3.09 & 23.44 \\
    SSE    &  1 & 2.15 & 33.11 \\
           &  4 & 2.15 & 33.04 \\
           & 32 & 2.15 & 33.07 \\
    AVX    &  1 & 1.27 & 37.53 \\
           &  4 & 0.91 & 52.42 \\
           & 32 & 0.86 & 55.84 \\
    \bottomrule
  \end{tabular}
  \caption{Execution statistics for algorithms on the GPP.}
  \label{tab:gpp:stats}
  \vspace{-3ex}
\end{wraptable}

For the simple and SSE implementations, the threading caused barely any overhead in the number of instructions or cycles.
For those implementations, the best performance is a factor 3.6 less than the single thread.
This is quite close to 4, the number of physical cores of the GPP.

The AVX implementation, however, sees less improvement with multiple threads.
The cause of this can be seen in the number of stalled cycles:
while it is 38 percent for the single thread, it is 56 percent for 32 threads.
The cause of this may be control hazards,
which would be peculiar because this does not show up for the other implementations.
Further investigation is necessary to establish the exact cause of the stalled cycles.
