X = csvread('matrix_x.csv');
Y = csvread('matrix_y.csv');

Correct = X*X + Y;
Multest = X*X;

R = zeros(13);

for j = 1:1:13
   C = zeros(13,1);
   for n = 1:1:13
       x = X(n,j);
       if n == j
           C(n) = C(n) + x^2;
       else
           for m = 1:1:13
               if m == n
                   C(m) = C(m) + x * (X(n,n) + X(j,j));
               else
                   C(m) = C(m) + x * X(m,n);
               end
           end
       end
   end
   for n = 1:1:13
       R(n,j) = C(n) + Y(n,j);
   end
end

for j = 1:1:13
   C = zeros(13,1);
   for n = 1:1:13
       x = X(n,j);
       if n == j
           C(n) = C(n) + x^2;
       else
           for m = 1:1:13
               if m == n
                   C(m) = C(m) + x * (X(n,n) + X(j,j));
               else
                   C(m) = C(m) + x * X(m,n);
               end
           end
       end
   end
   for n = 1:1:13
       R(n,j) = C(n) + Y(n,j);
   end
end
