\section{Methods}

\subsection{Simple matrix multiplication}
In order to be able to compare `improved' methods, a simple multiplication loop is implemented in \texttt{math\_simple()}, see appendix~\ref{app:math}.
This loop also adds $Y$ in an efficient manner by copying it to the result matrix ($R$). Thereby effectively clearing the result matrix and adding $Y$.

\subsection{Read matrix sequentially by transposition}
By transposing the input matrix $X$, all memory will be accessed sequentially.
In modern processors, this will improve performance because the processor pre-fetches memory data.
The ATmega328 does not do this, but has an internal memory pointer which can be incremented by one without extra computing time.

The transposition of the matrix will take $13\times 13 \times (2 + 2) = 676$ clock ticks, and will save $13\times 13 \times 13 = 2197$ clock ticks. Saving \SI{95}{\micro\second}.

The resulting C code can be seen in \texttt{math\_transpose()}, see appendix~\ref{app:math}.

\subsection{Read matrix in blocks by tiling}
Another thing that can save time is so-called \emph{loop tiling}.
This means extra for loops are added in order to try and get the loops into the processor's cache.
Because the Arduino has 32 8-bit registers instead of cache it is possible to optimize the reading of 4 by 4 blocks, resulting in 32 (two matrix blocks of 16) values which should be in the registers.
Unfortunately, these 32 8-bit registers also include a maximum of three memory pointers. For proper execution more are probably needed.

The $13\times 13$ size of the matrix presents a challenge however: it is not divisible into blocks of 4. This means extra logic has to be added to include these values. This opportunity is also used to add $Y$.

The resulting C code can be seen in \texttt{math\_tile()}, see appendix~\ref{app:math}.

\subsection{Reducing multiplications and loops}
As computing the square of a matrix of size $13 \times 13$ involves $13 \times 13 \times 13$ multiplications and $13 \times 13$ additions, attacking these computations and reducing them was the primary idea behind this method of optimization. Knowing that microcontroller Atmega328 takes 1 clock cycle for addition and 2 clock cycles for multiplication, reducing the number of multiplication seems to be a better idea. To implement this, we observed the behavior of multiplication for each element of the resulting matrix of $X_{13 \times 13} \cdot X_{13 \times 13}$.

Let $R_{13 \times 13}$ be the square of $X_{13 \times 13}$ matrix, $R_{13 \times 13} = X_{13 \times 13} \cdot X_{13 \times 13}$ then the diagonals of $R_{13 \times 13}$ be calculated as:
\begin{align*}
  R_{1,1} &= X_{1,1} \cdot X_{1,1 } + X_{1,2} \cdot X_{2,1 } + X_{1,3} \cdot X_{3,1 } + X_{1,4} \cdot X_{4,1} \dots X_{1,12} \cdot X_{12,1 } + X_{1,13} \cdot X_{13,1} \\
  R_{2,2} &= X_{2,1} \cdot X_{1,2 } + X_{2,2} \cdot X_{2,2 } + X_{2,3} \cdot X_{3,2 } + X_{2,4} \cdot X_{4,2} \dots X_{2,12} \cdot X_{12,2 } + X_{2,13} \cdot X_{13,1} \\
  \dots \\
  R_{12,12} &= X_{12,1} \cdot X_{1,12 } + X_{12,2} \cdot X_{2,12 } + X_{12,3} \cdot X_{3,12 } + X_{12,4} \cdot X_{4,12} \dots X_{12,12} \cdot X_{12,12 } + X_{12,13} \cdot X_{13,12} \\
  R_{13,13} &= X_{13,1} \cdot X_{1,13 } + X_{13,2} \cdot X_{2,13 } + X_{13,3} \cdot X_{3,13 } + X_{13,4} \cdot X_{4,13} \dots X_{13,12} \cdot X_{12,13 } + X_{13,13} \cdot X_{13,13}
\end{align*}
As seen from the calculation, the value $X_{1,2} \cdot X_{2,1}$ repeats in the calculation of $R_{1,1}$ as well as $R_{2,2}$. Similarly the value $X_{1,3} \cdot X_{3,1}$ repeats in the calculation of $R_{1,1}$ as well as $R_{3,3}$.To summarize, value $X{i,j} \cdot X_{j,i}$ repeats in the calculation of $R_{i,i}$ as well as $R_{j,j}$. This means there are $\binom{13}{2} = 78$ multiplications duplicating for $13 \times 13$ matrix square.
Removing this duplication saves about $78/(13 \cdot 13 \cdot 13) = 3.55\%$ of computational multiplication.

Considering 16MHz onboard crystal frequency, and 2 clock cycles for each multiplication, we save $(78 \cdot 2)/\SI{16}{MHz} = \SI{9.75}{µsec}$

As our presumption that the number of loops increases the overhead, we also tried to reduce the number of loops by computing two values in the same loop. This reduces the loop iteration to 50\%. The idea behind this was, instead of typically calculating $R_{i,j}$ in the “for  loop” where “$i$ and $j$ vary from 1 to 13”, we calculated $R_{i,j}$ and $R_{j,i}$ in a single loop using “for loop” where “$i$ varied from 2:13 and $j$ from $1:(j-1)$”.  So if we consider the two halves of matrix formed by the diagonal as two triangles, the loop goes on for one of the triangles of the matrix, but values are calculated for both of them.

The resulting C code can be seen in \texttt{math\_trian()}, see appendix~\ref{app:math}.

\subsection{Optimizing compiler flags}

The Arduino code is compiled with the GNU Compiler Collection (GCC).
GCC has various optimisation flags, which can be enabled in bulk by setting the \texttt{-O} flag. This flag accepts the following values shown in table~\ref{tab:oflag}.

\begin{wraptable}[10]{r}{.5\linewidth}
  \centering
  \caption{Optimisation options}
  \label{tab:oflag}
  \begin{tabular}{rl}
    \toprule
    option & description \\
    \midrule
    0 & No optimisation\tabularnewline
    1 & Optimize\tabularnewline
    2 & Optimize even more\tabularnewline
    3 & Optimize yet more\tabularnewline
    s & Optimize for size\tabularnewline
    fast & Disregard standards compliance\tabularnewline
    g & Optimize debugging experience\tabularnewline
    \bottomrule
  \end{tabular}
\end{wraptable}

By default the Arduino compiler is set to compile with \texttt{-Os} (for size).
Because the \texttt{-O2} and \texttt{-O3} settings promise improved performance, these will be compared to \texttt{-Os}.

Another option is manually setting a number of \texttt{-f} flags for improved performance.
This can be done by taking the best performing \texttt{-O} setting and adding the flags that the next level adds one by one.
