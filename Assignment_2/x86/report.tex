\section{General purpose processor}
\subsection{Introduction}
\begin{margintable}
  \begin{tabular}{r & r}
    \toprule
    Level & Size (\si{kB}) \\
    \midrule
    1 & 256 \\
    2 & 1024 \\
    3 & 6144 \\
    \bottomrule
  \end{tabular}
  \caption{Cache levels of the used processor}
\end{margintable}
% http://ark.intel.com/nl/products/65520/Intel-Core-i5-3570K-Processor-6M-Cache-up-to-3_80-GHz
The processor used for this part is an Intel Core i5-3570K\cite{i5}.
This is an x86 (CISC) architecture.
The most important features are:

\begin{itemize}
  \item Clock frequency of \SI{3.40}{GHz} to \SI{3.80}{GHz}.
  \item Three cache layers, see table~\ref{tab:cache}
  \item Extensions: SSE 4.1, 4.2 and AVX.
\end{itemize}

Because of SSE (Streaming SIMD Extensions) and AVX (Advances Vector Extensions),
the processor has build-in instructions to create vectors.

\subsection{Theory}
A matrix calculation $X\cdot Y$ results in fields which are the result of the dot product of the row- and column-vector of the input matrices.
This means that in the resulting matrix $R$, $R_{5,6}$ is the result of the dot product of the fifth row and sixth column.

Because a matrix multiplication is effectively a number of vector operations,
a GPP with support for vector operations should be able to gain significant performance gains, even on a 13 by 13 matrix.

Additional improvement can be gained by exploiting multi-threading.

\subsection{Method}
There are two main ways of implementing the matrix multiplication with the given GPP:
using a 128 bit vector with 16 or 32 bit components or using a 256 bit vector with 32 bit floating point components.

The performance is compared against a simple matrix multiplication algorithm.

\subsubsection{Integer: SSE}
The integer calculation is the hardest, because the intrinsics used for manipulating vectors do not easily support multiplications or memory access.
The calculation, implemented with \texttt{calc_sse} and \texttt{transpose} (see appendix~\ref{app:code:math}), is done in several steps:

\begin{itemize}
  \item Transpose X so vectors can be sequentially read from memory.
  \item Load one row from X as two 128 bits vectors with 16 bit elements.
  \item Loop trough the columns:
  \begin{itemize}
    \item Load a column as two 128 bits vectors with 16 bit elements.
    \item Multiply the row and column vectors and add adjacent pairs resulting in two 128 bits vectors with 32 bit elements.
    \item Keep adding adjacent pairs a couple of more times to produce the result.
    \item Load one row from Y as one 128 bit vector with 32 bit elements.
    \item Add the result and Y and store them in one row of result matrix R.
  \end{itemize}
\end{itemize}

The main problem with this approach is that loading the vectors from memory is done by loading separate elements, not reading a block of memory.

\subsubsection{Floating point: AVX}
For this approach the input matrix is stored as a floating point matrix instead of signed integers.
The calculation is implemented with \texttt{calc_avx} and \texttt{transpose_avx} (see appendix~\ref{app:code:math}).

Because single precision floating point numbers use 23 bits to store the value of the number (and 1 for the sign) and the maximum value of the result is only 17 bits no precision will be lost.
The approach with floating point numbers is a lot more straight forward than with integers:

\begin{itemize}
  \item Transpose $X$ so vectors can be sequentially read from memory in blocks of 4.
  \item Load one row from $X$ as two 256 bits vectors with 32 bit floating point elements.
  \item Loop trough the columns:
  \begin{itemize}
    \item Load a column as two 256 bits vectors with 32 bit floating point elements.
    \item Calculate the dot product of the 128 bit components of the vectors.
    \item Add the results from the dot products together to get a 128 bit vector containing only one value.
    \item Store this value in a temporary 256 bit vector $Z$.
    \item Load half a row from $Y$ as one 256 bits vector with 32 bit floating point elements.
    \item Add $Z$ and $Y$ and store them in half a row of result matrix $R$.
  \end{itemize}
\end{itemize}

This approach has some inefficiencies (vectors are being added while they contain only one value),
but is relatively easy to understand.

\subsubsection{Multithreading}
For multithreading the POSIX thread library \texttt{pthread} is used.
Threads are created and joined in simple for loops.
No measures are taken against hazards when writing to the result matrix $R$.
Cases are made with 1, 4 and 32 threads.

\subsubsection{Measuring performance}
Performance is measured in several ways:

\begin{itemize}
  \item \texttt{main.c}, which tries a varying number of threads and measures the time the calculations take. This is measured for \number{1000000} calculations.
  \item Valgrind's \emph{cachegrind}, which shows information about instructions and cache access.
  \item Linux's \emph{perf stat}, which shows runtime information about a process.
\end{itemize}

For each way of calculating the code is compiled using GCC with options \texttt{-Wall -march=native -O3}. This shows warnings, compiles for the entire architecture of the GPP compiling the code and optimizes for fast execution.

\subsection{Execution}
\begin{table}
  \begin{tabular}{l r r r r r}
    \toprule
    Algoritm & Threads & Time (\si{ms}) & Instructions & Cycles & Stalled Cycles \\
    \midrule
    Simple &  1 &  706 &  8,328,100,256 & 2,671,090,095 &   607,246,703 \\
           &  4 &  199 &  8,328,215,049 & 2,699,342,914 &   630,821,752 \\
           & 32 &  192 &  8,328,268,868 & 2,698,323,350 &   632,579,353 \\
    SSE    &  1 & 1614 & 12,904,100,247 & 6,015,991,848 & 1,991,603,603 \\
           &  4 &  468 & 12,904,215,013 & 6,012,000,868 & 1,986,664,366 \\
           & 32 &  446 & 12,904,268,602 & 6,016,496,474 & 1,989,450,645 \\
    AVX    &  1 &  536 &  2,683,100,247 & 2,117,374,999 &   794,690,966 \\
           &  4 &  230 &  2,683,215,013 & 2,970,627,544 & 1,557,125,356 \\
           & 32 &  225 &  2,683,268,580 & 3,146,242,640 & 1,756,740,411 \\
    \bottomrule
  \end{tabular}
  \caption{Results of 1 million calculations on the GPP.}
  \label{tab:gpp:res}
\end{table}

\begin{table}
  \begin{tabular}{l r r r}
    \toprule
    Algoritm & Threads & Inst./cycle & Stalled Cycles (\%) \\
    \midrule
    Simple &  1 & 3.12 & 22.73 \\
           &  4 & 3.09 & 23.37 \\
           & 32 & 3.09 & 23.44 \\
    SSE    &  1 & 2.15 & 33.11 \\
           &  4 & 2.15 & 33.04 \\
           & 32 & 2.15 & 33.07 \\
    AVX    &  1 & 1.27 & 37.53 \\
           &  4 & 0.91 & 52.42 \\
           & 32 & 0.86 & 55.84 \\
    \bottomrule
  \end{tabular}
  \caption{Execution statistics for algorithms on the GPP.}
  \label{tab:gpp:stats}
\end{table}

\section{Results}
The fastest achieved result is \SI{192}{ns} per calculation.


\begin{table}
  \begin{tabular}{r r}
    Cycles & Speed (\si{MHz})\\
    \midrule
      1 & 5.21 \\
     13 & 67.7 \\
    169 & 880  \\
    \bottomrule
  \end{tabular}
  \caption{FPGA clock speed required for number of calculation cycles}
  \label{tab:fpga}
\end{table}

\section{Conclusion}
