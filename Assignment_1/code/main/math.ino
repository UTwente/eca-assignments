/* Simple algoritm
 * Time: 3461 μs
 *
 * This is the default way of calculating matrix multiplication.
 *
 * Compiler (O) flag settings:
 * | flag | time (μs) |
 * | s    | 11489     |
 * | 1    | 3482      |
 * | 2    | 3461      |
 * | 3    | 4770      |
 * | 2+   | 3384      |
 */
void calc_simple() {
  byte i,j,k;

  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
      R[i][j] = Y[i][j];
      for (k = 0; k < 13; k++) {
        R[i][j] += X[i][k] * X[k][j];
      }
    }
  }
}

/* Simple algoritm with transposition first
 * Time: 3467 μs
 *
 * This improves the sequential reading of data.
 * Making it easier to prefetch, thus increasing performance.
 *
 * The transpose takes about 220 μs.
 *
 * Compiler (O) flag settings:
 * | flag | time (μs) |
 * | s    | 10265     |
 * | 2    | 3552      |
 * | 3    | 3467      |
 * | 2+   | 3401      |
 */
void calc_transpose() {
  byte i,j,k;

  // Transpose matrix X and initialise R
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
      T[i][j] = X[j][i];
      R[i][j] = Y[i][j];
    }
  }

  // Multiply matrix X with T and add Y
  for (i = 0; i < 13; i++) {
    for (j = 0; j < 13; j++) {
        for (k = 0; k < 13; k++) {
          R[i][j] += X[i][k] * T[j][k];
      }
    }
  }
}

/* Algoritm with loop tiling.
 * Time: 3974 μs
 *
 * Based on:
 *   What Every Prormammer Should Know About Memory, Ulrich Drepper, 2007
 *
 * Compiler (O) flag settings:
 * | flag | time (μs) |
 * | s    | 9246      |
 * | 2    | 5767      |
 * | 3    | 3974      |
 * | 2+   | 5591      |
 */
void calc_tile() {
  #define TS 4

  byte i, i2, j, j2, k, k2;
  long* rR;
  byte* rX1;
  byte* rX2;

  // Calculate values not calculated in the tiling and add Y
  R[12][12] = X[12][12] * X[12][12] + Y[12][12];
  for (i = 0; i < 12; ++i) {
    R[i][12]   = X[i][12]  * X[12][12] + Y[i][12];
    R[12][i]   = X[12][12] * X[12][i]  + Y[12][i];
    R[12][12] += X[12][i]  * X[i][12];

    for (j = 0; j < 12; ++j) {
      R[i][j]   = X[i][12] * X[12][j] + Y[i][j];
      R[i][12] += X[i][j]  * X[j][12];
      R[12][i] += X[12][j] * X[j][i];
    }
  }

  // Multiply the inner 12x12 matrix in pieces of 4x4
  for (i = 0; i < 12; i += TS) {
    for (j = 0; j < 12; j += TS) {
      for (k = 0; k < 12; k += TS) {
        for (i2 = 0, rR = &R[i][j], rX1 = &X[i][k];
             i2 < TS; ++i2, rR += 13, rX1 += 13) {
          for (k2 = 0, rX2 = &X[k][j]; k2 < TS;
               ++k2, rX2 += 13) {
            for (j2 = 0; j2 < TS; ++j2) {
              //rR[j2] += rX1[k2] * rX2[j2];
              rR[j2] += X[i+i2][k+k2] * rX2[j2];
              //R[i+i2][j+j2] += X[i+i2][k+k2] * X[k+k2][j+j2];
            }
          }
        }
      }
    }
  }
}

/* Optimisation by reducing multiplications.
 * Time: 7108 μs
 *
 * Compiler (O) flag settings:
 * | flag | time (μs) |
 * | s    | 13771     |
 * | 2    | 7744      |
 * | 3    | 7108      |
 * | 2+   | 7134      |
 */
void calc_trian() {
  #define N 12

  byte a,b,i,j;
  long temp;

  // Initialise diagonals
  for (a = 0; a < N; ++a) {
    R[a][a] = Y[a][a];
  }

  // Calculate diagonals
  for (a = 0; a < N; ++a) {
    R[a][a] += X[a][a] * X[a][a];

    for (b = a + 1; b <= N; ++b) {
  		temp = X[a][b] * X[b][a];
  		R[a][a] += temp;
  		R[b][b] += temp;
  	}
  }

  // Initialise corners
  R[N][N] = Y[N][N] + X[N][N] * X[N][N];

  // Calculate the remaining half triangles
  for (i = 1; i <= N; ++i) {
		for (j = 0; j < i; ++j) {
      R[i][j] = Y[i][j];
      R[j][i] = Y[j][i];
      for (a = 0; a <= N; ++a) {
			  R[i][j] += X[i][a] * X[a][j];
			  R[j][i] += X[j][a] * X[a][i];
			}
		}
  }
}
